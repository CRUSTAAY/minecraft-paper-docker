import json,urllib.request,hashlib
from typing import List, Tuple

loadedfiles = False
requireconfig = False

configfile = "/minecraft/dockerconfig.json"
defaultconfigfile = "/dockerdefaultconfig.json"
datafile = "/minecraft/dockerdata.json"
defaultdatafile = "/dockerdata.json"

config = None
defaultconfig = None
data = None
defaultdata = None

def check_loaded_files():
  if(not loadedfiles):
    load_files()

def save_config():
  import os
  os.remove(configfile)
  with open(configfile,"a+") as _configfile:
    _configfile.write(config)
  print("Saved config file")

def save_data():
  import os
  os.remove(datafile)
  with open(datafile,"a+") as _datafile:
    _datafile.write(json.dumps(data))
  print("Saved data file")

def require_config() -> bool:
  return requireconfig

def rebuild_defaults(reqfile: str):
  import copy
  if(reqfile == "configfile"):
    global config, requireconfig
    config = copy.copy(defaultconfig)
    requireconfig = True
    with open(configfile,"a+") as _configfile:
        _configfile.write(json.dumps(config))
  elif(reqfile == "datafile"):
    global data
    data = copy.copy(defaultdata)
    with open(datafile,"a+") as _datafile:
        _datafile.write(json.dumps(data))



def load_files():
  global config, defaultconfig, data, defaultdata
  with open(defaultconfigfile) as _defaultconfigfile:
    defaultconfig = json.loads(_defaultconfigfile.read())
  
  with open(defaultdatafile) as _defaultdatafile:
    defaultdata = json.loads(_defaultdatafile.read())

  #make sure user config exists, creating new config if it doesnt from defaults
  from os.path import exists
  if(not exists(configfile)):
    rebuild_defaults("configfile")
  else:
    with open(configfile) as _configfile:
      config = json.loads(_configfile.read())

  #now with data file
  if(not exists(datafile)):
    rebuild_defaults("datafile")
  else:
    with open(datafile) as _datafile:
      data = json.loads(_datafile.read())
    
  global loadedfiles
  loadedfiles = True
def get_config(name: str) -> str:
  if not loadedfiles:
    load_files()
  return config[name]

def set_config(name: str, val: str):
  if not loadedfiles:
    load_files()
  config[name] = val

def get_data(name: str) -> str:
  if not loadedfiles:
    load_files()
  return data[name]

def set_data(name: str, val: str):
  if not loadedfiles:
    load_files()
  data[name] = val


def get_str_env(name: str, default_v: str) -> str:
    """Gets string Environment variable, normally for docker
    ### Parameters
    name : str
      - Env variable name
    default_v : str
      - Default variable if blank/unset
    ### Returns
    string
      - value contained within ENV[name]
    """
    import os
    return os.getenv(name,str(default_v))

def set_str_env(name: str, val: str):
    """Sets Environment variable, normally for docker
    ### Parameters
    name : str
      - Name of variable to be set
    val : str
      - Value of variable to be set
    ### Returns
    None
    """
    import os
    os.environ[str] = val

####For MC Paper

def get_mcpaper_latest_build(ver: str) -> int:
    """Gets Minecraft Paper's latest build for a given version
    #### Parameters
    ver : str
      - Version of minecraft paper to fetch latest build number
    #### Returns
    int
      - Build number of the latest build for the given version [ver]
    """
    jparsed = json.loads(  urllib.request.urlopen("https://api.papermc.io/v2/projects/paper/versions/" + ver + "/builds")  .read())
    return int(jparsed["builds"][-1]["build"])

def get_mcpaper_all_build_l(ver: str) -> List[int]:
    """Gets all Minecraft Paper builds for a given version
    #### Parameters
    ver : str
      - Version of minecraft paper to fetch list of build numbers
    #### Returns
    List[int]
      - List of numbers representing builds for version [ver]
    """
    r = []
    jparsed = json.loads(  urllib.request.urlopen("https://api.papermc.io/v2/projects/paper/versions/"+ver+"/builds")  .read())
    for i in jparsed["builds"]:
        r.append(int(i["build"]))
    return r

def get_mcpaper_sup_ver_l() -> List[str]:
    """Gets all supported versions of minecraft paper
    #### Returns
    List[str]
      - List of version numbers
    """
    r = []
    jparsed = json.loads(  urllib.request.urlopen("https://api.papermc.io/v2/projects/paper")  .read())
    for i in jparsed["versions"]:
        r.append(i)
    return r

def get_mcpaper_latest_ver() -> str:
    """Gets latest version of minecraft paper
    #### Returns
    str
      - Latest version number
    """
    jparsed = json.loads(  urllib.request.urlopen("https://api.papermc.io/v2/projects/paper")  .read())
    return jparsed["versions"][-1]

def get_mcpaper_target(ver: str, build: int) -> Tuple[str,str]:
  jparsed = json.loads(  urllib.request.urlopen("https://api.papermc.io/v2/projects/paper/versions/"+ ver +"/builds/" + str(build))  .read())
  checksum = jparsed["downloads"]["application"]["sha256"]
  name = jparsed["downloads"]["application"]["name"]
  return (name,checksum)


def get_mc_cur_ver() -> str:
    """Gets current installed version of minecraft server software
    #### Returns
    str
      - current installed version string
    """
    return get_data("mcversion")

def get_mc_cur_build() -> int:
    """Gets current installed build of minecraft server software if valid
    #### Returns
    int
      - Current installed build int
    """
    return int(get_data("mcbuild"))

def set_mc_cur_ver(version: str):
    """sets current installed version of minecraft server software
    #### Parameters
    version : str
      - Version of minecraft server software to be used
    """
    set_data("mcversion",version)

def set_mc_cur_build(build: int) -> bool:
    """Gets current installed build of minecraft server software
    #### Parameters
    build : int
      - build number of minecraft server software to be used
    #### Returns
    bool
      - true if success in setting
    """
    set_data("mcbuild",build)


def download_mcpaper_jar(version: str, build: int, target:str, dest: str, checksum: str) -> bool:
    #grab the file
    url = "https://api.papermc.io/v2/projects/paper/versions/"+ version +"/builds/"+ str(build) +"/downloads/" + target
    urllib.request.urlretrieve(url,dest)
    #check its hash
    with open(dest,"rb") as f:
        hash = hashlib.sha256(f.read()).hexdigest()
        if(hash == checksum):
            return True
        else:
            return False

def delete_mc_jar(jar: str):
    import os
    os.remove(jar)

def mc_update():
  mc_paper_update()

def mc_paper_update():

    
    tv = get_config("targetversion")
    tb = get_config("targetbuild")

    #version check
    if(tv == "latest"):#latest version
      set_data("mcversion",get_mcpaper_latest_ver())

    elif(tv in get_mcpaper_sup_ver_l()):#specific version
      set_data("mcversion",tv)
            
    else:#unknown or unsupported entry
      raise Exception("Unknown or unsupported paper version {version} ({versionres})".format(get_config("targetversion"), versionres = get_data("mcversion")))

    #build check
    if(tb == "latest"):
      set_data("mcbuild",(get_mcpaper_latest_build(get_data("mcversion"))))
    elif(int(tb) in get_mcpaper_all_build_l(get_data("mcversion"))):
      set_data("mcbuild",tb)
    else:
      raise Exception("Unknown or unsupported paper build {build} for version {version} ({versionres})".format(build=tb, version=get_config("targetversion"), versionres = get_data("mcversion")))
    
    #target and checksum fetch
    tn, tc= get_mcpaper_target(get_data("mcversion"),get_data("mcbuild"))
    set_data("mctargetjar",tn)
    set_data("mctargetchecksum",tc)
    save_data()

    #download check
    if(not download_mcpaper_jar(get_data("mcversion"),int(get_data("mcbuild")),get_data("mctargetjar"),"/minecraft/server.jar",get_data("mctargetchecksum"))):
        print("Error downloading, trying again")
        if(not download_mcpaper_jar(get_data("mcversion"),int(get_data("mcbuild")),get_data("mctargetjar"),"/minecraft/server.jar",get_data("mctargetchecksum"))):
            raise Exception("Update failed!\nEither hash or file download failed")
