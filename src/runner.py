import sys
from time import sleep
from scripts import *
from signal import signal, SIGINT, SIGTERM
from threading import Thread
from os import getenv
import pexpect

DEBUG = getenv("debug",False)
def sig(signalrec, frame):
    print("got sig: " + str(signalrec))
    global mcprocess
    if((signalrec == SIGINT) or (signalrec == SIGTERM)):
        if(mcprocess == None):
            return
        else:
            mcprocess.sendintr()
            print("Shutting down due to signal (Most likely the user or docker itself commanded to stop this container)")
            mcprocess.wait()
            print("shut down")
            exit()

#lil hack to get logfile to output to console live without blocking input
def outputter():
    while True:
        try:
            mcprocess.expect('\n')
        except pexpect.EOF:
            break
        except pexpect.TIMEOUT:
            pass


def inputter():
    while True:
        inp = input()
        mcprocess.sendline(inp)



#prep
print("starting")
signal(SIGINT, sig)
signal(SIGTERM, sig)

load_files()

if(require_config() and not DEBUG):
    raise Exception("Config file just created, please check config.json and complete as required")

#update mc check
if(get_config("autoupdate")):
    print("Autoupdate enabled, checking for updates")
    mc_update()
    pass
else:
    if(get_data("mcversion") == ""):
        raise Exception("Unset data variables\n If you are not using auto update please enter correct values into data.json and ensure server .jar is in correct location!")

    print("Autoupdate disabled, continuing with version {version} {build}".format(version=get_data("mcversion"),build=get_data("mcbuild")) )
    pass


#prestart
eula = ""
if(get_config("argmojangeula")):
    eula = "-Dcom.mojang.eula.agree=true"
    pass
pre = "-Xms{mem} -Xmx{mem} {argruntime} {el} -jar /minecraft/server.jar --universe {worlddir} {argsupplementary}"
startupstring = pre.format(mem=get_config("memory"),argruntime=get_config("argruntime"),el = eula,worlddir=get_config("worlddir"),argsupplementary=get_config("argsupplementary"))
startup = startupstring.split()

#start
print("Starting")

mcprocess = pexpect.spawnu(command="java",args=startup,cwd="/minecraft/")
sleep(2)

#lil hack, see  def outputter():
mcprocess.logfile_read = sys.stdout

#io threads
opt = Thread(target=outputter, name="OutputThread")
ipt = Thread(target=inputter, name="InputThread")
#makes them close when main thread closes
opt.setDaemon(True)
ipt.setDaemon(True)
#start threads
opt.start()
ipt.start()
#main thread waits for process to die
while (mcprocess.isalive() == True):
    sleep(1)
else:
    #Kill the threads,
    #and not just the main, 
    #but the children, too! 
    #They're like animals, 
    #and i slaughtered them like animals!
    #I HATE THEM!
    sys.exit()
