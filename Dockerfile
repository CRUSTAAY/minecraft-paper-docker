FROM openjdk:20-buster


#install packages
RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install curl vim jq cron tzdata python3 python3-pip && \
    apt-get -y clean && \
    rm -rf /var/lib/apt/lists/*

ENV TZ=Etc/GMT+0

WORKDIR /
RUN mkdir /minecraft

COPY /src/runner.py /runner.py
COPY /src/defaults.json /dockerdefaultconfig.json
COPY /src/data.json /dockerdata.json
COPY /src/scripts.py /scripts.py
RUN chmod +x /runner.py
RUN pip3 install pexpect

EXPOSE 25565
VOLUME ["/minecraft"]


ENTRYPOINT ["/usr/bin/python3", "-u" ,"/runner.py"]